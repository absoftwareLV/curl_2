$(document).on('mouseover','.more-link',function(){ $(this).addClass('hover'); });
$(document).on('mouseout','.more-link',function(){ $(this).removeClass('hover'); });
$(document).on('click','.onHover',function() { 
	$(this).find('a')[0].click();
});

$(document).on('click','.filter',ajaxFilter);
$(document).on('click','.filter-parent',filterOpen);

$(document).on('ready',start);

var xhr = new XMLHttpRequest();
var preloader = document.createElement('DIV');
	preloader.setAttribute('id','abs_preloader');
function start () {

	checkOptions();

}

function checkOptions() {
	
	if ( $('.options').length>0 ) {
		$('.options').each(function() {
			$(this).find('p').last().detach();
		});
	}
	
}

function filterOpen(e) {
	var li = $(this);
	if (e.target.tagName == 'H3') {
		if (li.hasClass('active')) li.removeClass('active');
		else li.addClass('active');
	}
}

function ajaxFilter() {
	var filter = $(this);
	var form = filter.closest('form')[0];
	var data = new FormData(form);
	var p = $('#resources .catalogue');
	p.append(preloader);
	xhr.open('POST','/home/printeri/curl/ajax.php?a=filter');
	xhr.send(data);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
		 if(xhr.status == 200) {
		   p.html(xhr.responseText);
		 }
		}
	};
	
}