<div class="prece">
	<div class="nosaukums">{FIRMA}<span>{MODELIS}</span></div>
	<div class="bilde"><img src="{BILDE}" alt="{MODELIS}"/></div>
	<div class="cena">{CENA}</div>
	<div class="cena_old">{CENA_OLD}</div>
	<div class="akcija">{AKCIJA}</div>
	<div class="onHover">
		<span class="old">{CENA_OLD}</span>
		<span class="new">{CENA}</span>
		<a href="{SAITE}" target="_blank" alt="{MODELIS}">Uzzināt vairāk</a>
	</div>
	{OPTIONS}
</div>