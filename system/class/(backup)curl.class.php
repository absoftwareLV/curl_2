<?php
	
	define(ABSDIR, realpath(dirname(__FILE__)));
	$tmp = explode('/',ABSDIR);
	for ($i=0;$i<count($tmp)-2;$i++) 
		$new .= '/' . $tmp[$i];
	define(TPLDIR, $new . '/tpl');
	
	
	function __autoload ( $class ) {
		require_once ABSDIR . '/' . $class . '.class.php';
	}
	
	class curl {
	
		private $data;
		protected $size = 0;
		public $url;
		public $options = array();
     public $titles = array();
		protected $filters;
		
		public function __construct() {

		}
		
		public function setURL ( $url ) {
			$this->url = $url;
			$this->url = str_replace(
				array("[", "]"),
				array("%5B", "%5D"),
				$this->url
			);
		}
		
		public function setSIZE ( $size ) {
			$this->size = $size;
		}
		
		public function setFILTERS( $filters ) {
			$this->filters = $filters;
		}
		
		public function prepare () {
		
		  $ch = curl_init();
		  $timeout = 5;
		  curl_setopt($ch, CURLOPT_URL, $this->url);
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		  
		  $this->data = curl_exec($ch);
		  curl_close($ch);	  		

		}
		
		public function getProducts ( $veikals ) {
		
			$this->prepare();
		
			$preces = $objects = array();
			$i = 0;
			
			if ($veikals != '220') {
			
				$lapasURL = 'http://www.rdveikals.lv/';				
				preg_match_all('/product_box_back_top.gif(.*?)product_box_back_bottom.gif/s', $this->data, $pRows); //katrs produkts
				foreach ( $pRows[1] as $product ) {
				
					preg_match_all( '/<div class="product_name">(.*?)<\/div>/s', $product, $nosaukumi ); //katra produkta bloki ar datiem
					preg_match_all( '/<div class="photo_box">(.*?)<\/div>/s', $product, $bildes );
					preg_match_all( '/<div class="price_part_1">(.*?)<\/div>/s', $product, $cenas );
					
					preg_match_all( '/<s>(.*?)<\/s>(.*$)/s', $cenas[1][0], $cena ); //no katra bloka iegustam nepieciesamos datus
					preg_match_all( '/<a href="(.*?)">(.*?)<\/a>(.*$)/s', $nosaukumi[1][0], $header );
					preg_match_all( '/<img src="(.*?)"/s', $bildes[1][0], $bilde );
					
					$objects[$i]['cena'] = $cena[2][0];
					$objects[$i]['cena_old'] = $cena[1][0];
					
					$objects[$i]['saite'] = 'http://go.doaffiliate.net/rdveikals.lv/kredits/3897';
					$objects[$i]['modelis'] = $header[2][0];
					$objects[$i]['firma'] = $header[3][0];
					
					$objects[$i]['bilde'] = $lapasURL . $bilde[1][0];
					$objects[$i]['options'] = '';
					
					$i++;
				}
			
			} else {					

				$lapasURL = 'http://adafi.hit.gemius.pl/hitredir/id=p4NK4T9Za878xwIR9zkNjdUKLSn1xcMwkmCc_GIZser.d7/stparam=oplknknmna/url=';
				preg_match_all('/<div class="fakeProductContainer">(.*?)<\/div>\n\n\n\t<form/s', $this->data, $pRows);
				foreach ($pRows[1] as $product) {
					
					preg_match_all('/class="basePrice">(.*?)<\/span>/s', $product, $cena);
					preg_match_all('/<h3>\n\t\t<a href="(.*?)" title="(.*?)">(.*?)<\/a>/s', $product, $header);
					preg_match_all('/img src="(.*?)" title/s', $product, $bilde);
					preg_match_all('/<p class="descFields">(.*?)<\/p>/s', $product, $options);
					$t = true;
					foreach ( $this->filters as $group=>$vals ) {
						if ( $this->strposArr($product, $vals, 1) === false ) {
							$t = false;
						}
					}
					if ($t === true) {
						$objects[$i]['cena'] = $cena[1][0];
						$objects[$i]['cena_old'] = $cena[1][1];
						
						// var_dump($header);

						$objects[$i]['saite'] = $lapasURL . 'http://www.220.lv/' . $header[1][0];
						$objects[$i]['firma'] = $header[3][0];
              $this->titles[] = $header[3][0];
						$objects[$i]['modelis'] = '';						              
            
						$objects[$i]['bilde'] = $bilde[1][0];
						$this->options[] = $objects[$i]['options'] = '<div class="options"><h1>Parametri</h1><p>' . str_replace( '</em>','</em></p><p>',$options[1][0] ) . '</p></div>';					
						
						$i++;
					}
				}				
			}
			
			( $this->size > 0 ) ? $size = $this->size : $size = count($objects);			
			$preces[] = '<div class="catalogue">';
			for ($i=0;$i<$size;$i++) {
			  if ($i<count($objects)) {

			  	$inc = get_included_files();
			  	if (!in_array( 
			  		ABSDIR . '/prece.class.php',
			  		$inc
			  	))
			  	{
			  		require_once ABSDIR . '/prece.class.php';
			  	}

				$preces[] = new prece( $objects[$i] );
			  }
			}
			
			$preces[] = '<div class="clear"></div>';
			$preces[] = '</div>';
			print implode($preces);			
		}

		private function strposArr($haystack, $needles=array(), $offset=0) {
			$t = false;
			foreach($needles as $needle) {
            $needle = preg_replace('/[^A-Za-z0-9\-]/', '', $needle);
					$res = strpos(str_replace('&amp;quot;','',htmlspecialchars($haystack)), str_replace('"','',$needle));
					if ($res !== false) $t = true;
			}
			return $t;		
		}
		
	}
	
?>