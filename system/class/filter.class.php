<?php

	class filter extends curl {	
	
     public $options;
     public $filters;
     public $saite;
     public $titles;
	
		public function __construct( $options, $saite, $titles = false ) {
			
			$this->options = $options;
			$this->saite = $saite;
       $this->titles = $titles;
			
			$this->sortFilters();
		}
		
		public function sortFilters() {
			
      $this->filters['titles'][] = 'Firma';
       foreach ( $this->titles as $full_title )
       {
         $title = explode(" ", $full_title);
         if (count($title))
         {
           $title = str_replace(" ", "", $title[0]);
         }
         
         if ( !in_array($title, $this->filters['Firma']) )
         {
           $this->filters['Firma'][] = $title;
         }
         
       }    																	
			
			foreach ( $this->options as $opt ) {
				
				preg_match_all(
					'/<div class=\"product-labels-description\">(.*?)<\/div>/s',
					$opt,
					$newOptions
				);
				$newOptions = $newOptions[1][0];
				
				preg_match_all('/(.*?)<b>(.*?)<\/b>/s', $newOptions, $row, PREG_SET_ORDER);
				foreach ( $row as $r ) 
				{	
					$value = $r[2];
					$type = str_replace(':','',$r[1]);
					$ttype = str_replace(' ','',strtolower($type));
					if (!in_array($ttype,$this->filters['s']) && $ttype!='') {
						$this->filters['s'][] = $ttype;
						$this->filters['titles'][] = $type;
					}
					if (!in_array($value,$this->filters[$type]))
						$this->filters[$type][] = $value;
				}								
				
			}
			
		}
		
		public function getFilters() {
			$tpl = file_get_contents(TPLDIR . '/filter.tpl');			
			preg_match_all('/<ul>(.*?)<\/ul>/s',$tpl,$child);
			$html[] = '<ul class="outer-filter">';
			foreach ( $this->filters['titles'] as $title ) {
				$li = array();				
				foreach ( $this->filters[$title] as $val ) {
					$v = str_replace(array('{VALUES}','{TITLES}'),array($val,$title),$child[1][0]);
					$li[] = $v;
				}
				$li = implode($li);
				$html[] = str_replace(array('{TITLES}','<li><label><input class="filter" type="checkbox" value="{VALUES}" name="'.$title.'[]"/>{VALUES}</label></li>'), array($title,$li), $tpl);
			}
			$html[] = '</ul>';
			
			print implode($html) . '<input class="hidden" name="url" value="'.$this->saite.'"/>';
		}
		
	}

?>