<?php

	class prece {
	
		private $prece;
		private $tpl;
	
		public function __construct ( $prece ) {
						
			$this->tpl = file_get_contents( TPLDIR . '/prece.tpl' );
			$this->prece = $prece;
			
		}
		
		public function __toString () {
		
			foreach ( $this->prece as $key=>$val ) {
				$this->tpl = str_replace( '{'. strtoupper($key) .'}', $val, $this->tpl );
			}
			
			if ( $this->prece['cena_old'] != "" ) {
				$this->tpl = str_replace( '{AKCIJA}','<span>Akcijas prece!</span>', $this->tpl );
			} else {
				$this->tpl = str_replace( '{AKCIJA}','<span class="popular">Populāra prece!</span>', $this->tpl );
			}
			
			return $this->tpl;
		
		}
	
	}

?>